# simple-spring-boot-starter

#### 介绍
手写一个starter应用实例

#### 应用实例背景
在SpringCloud微服务架构中，往往需要在A服务中调用B服务。例如：营销微服务ms-marketing需要调用用户微服务 ms-user中的接口。

![输入图片说明](https://foruda.gitee.com/images/1661422832974301023/79ddb714_7798414.png "屏幕截图")

#### 痛点：ms-marketing如何“一站式”调用ms-user中的接口
- 不用关心被调用接口的uri
- 不需要自行构建被调用接口的参数和返回值的类型

#### 解决方案：使用SpringBoot中的自动配置，构建 ms-user 的starter

#### 参考链接：
【硬核干货！SpringBoot自动配置实战项目，从0开始手撸Starter，零基础小白可全程跟学#安员外很有码】
https://www.bilibili.com/video/BV1Zu4116714?vd_source=9a5e0b25634e26cf2e0ccc09fabf93ff


#### 什么是starter
starter是一个“一站式服务”的依赖jar包：
- 包含了Spring以及相关技术的所有依赖
- 提供了自动配置的功能，开箱即用
- 提供了良好的依赖管理，避免了包遗漏，版本冲突等问题

#### starter的结构图
![输入图片说明](https://foruda.gitee.com/images/1661422905902506900/71edaff5_7798414.png "屏幕截图")