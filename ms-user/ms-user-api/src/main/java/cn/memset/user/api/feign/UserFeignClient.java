package cn.memset.user.api.feign;

import cn.memset.user.api.dto.User;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author lixing
 * @date 2022/8/25
 */

@FeignClient(name = "ms-user",
        url = "${cnmemset.ms-user.url:127.0.0.1:8080}",
        fallbackFactory = UserFeignClient.UserFallbackFactory.class)
public interface UserFeignClient {
    @GetMapping("/get/{userId}")
    @ResponseBody
    public User getUserById(@PathVariable("userId") long userId);

    /**
     * OpenFeign提供的服务降级：服务接口不可用或调用失败的时候，调用降级接口
     */
    @Component
    public static class UserFallbackFactory implements FallbackFactory<UserFeignClient>{
        @Override
        public UserFeignClient create(Throwable cause) {
            return new UserFeignClient() {
                @Override
                public User getUserById(long userId) {
                    System.out.println("调用ms-user发生异常：" + cause);
                    return null;
                }
            };
        }
    }
}
