package cn.memset.user.web.controller;

import cn.memset.user.api.dto.User;
import cn.memset.user.api.feign.UserFeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lixing
 * @date 2022/8/25
 */
@RestController
public class UserController implements UserFeignClient {
    private static final Map<Long, User> users = new HashMap<>();

    static {
        users.put(100L, new User(100L, "张三"));
        users.put(101L, new User(101L, "李四"));
    }

    @Override
    @GetMapping("/get/{userId}")
    public User getUserById(@PathVariable("userId") long userId) {
        return users.get(userId);
    }
}
