package cn.memset.user.autoconfigure;

import cn.memset.user.api.feign.UserFeignClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author lixing
 * @date 2022/8/25
 */
@Configuration(proxyBeanMethods = false)
@EnableFeignClients(basePackages = "cn.memset.user.api.feign")
@EnableConfigurationProperties
@Import({UserFeignClient.UserFallbackFactory.class})
public class MsUserAutoConfiguration {
}
